#!/bin/sh -x
{
echo '<?xml version="1.0" encoding="UTF-8"?>'
echo '<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">'
echo '<book lang="en">'
cat 00_bookinfo.rawxml
cat 00_preface.rawxml
cat 01_gnu_linux_tutorials.rawxml
cat 02_debian_package_management.rawxml
cat 03_the_system_initialization.rawxml
cat 04_authentication.rawxml
cat 05_network_setup.rawxml
cat 06_network_applications.rawxml
cat 07_the_x_window_system.rawxml
cat 08_i18n_and_l10n.rawxml
cat 09_system_tips.rawxml
cat 10_data_management.rawxml
cat 11_data_conversion.rawxml
cat 12_programming.rawxml
cat 90_appendix.rawxml
echo '</book>'
} > debian-reference.rawxml
